const { spawn } = require("child_process");

// const childpython = spawn("python", ["--version"]);
const childpython = spawn("python", ["pymod.py"]);

childpython.stdout.on("data", (data) => {
  console.log(`Output: \n${data}`);
});

childpython.stderr.on("data", (data) => {
  console.error(`Error: ${data}`);
});

childpython.on("close", (code) => {
  console.log(`child process exited with code ${code}`);
});